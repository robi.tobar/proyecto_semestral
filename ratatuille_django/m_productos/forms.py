from django import forms
from django.forms import fields
from tempus_dominus.widgets import DatePicker, TimePicker, DateTimePicker
from m_productos.models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class frmProducto(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ('id_categoria','nombre','precio','imagen') #campos del objeto Producto

class frmModProducto(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ('nombre','precio','imagen') #campos del objeto Producto

class frmProductoCarro(forms.ModelForm):
    class Meta:
        model = Carrito
        fields = ('id_producto','cantidad')

class frmReserva(forms.ModelForm):
    fecha = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'useCurrent': True,
                'collapse': False,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            }
        ),label = 'Fecha'
    )
    class Meta:
        model = Reserva
        fields = ('comensales',)

class frmR(forms.ModelForm):
    class Meta:
        model = Reserva
        fields = ('mesa','comensales','precio','fecha')
        
class frmR2(forms.ModelForm):
    class Meta:
        model = Reserva
        fields = ('fecha','estado')

class frmUsuario(UserCreationForm):
    class Meta:
        model = User
        fields = '__all__'

class frmMesa(forms.ModelForm):
    class Meta:
        model = Mesa
        fields = ('numero',)

class frmEstadoRes(forms.ModelForm):
    class Meta:
        model = Reserva
        fields = ('estado',)

class frmEstadoPro(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ('estado',)

