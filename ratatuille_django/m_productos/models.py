from django.db import models
from django.forms import fields
from django import forms

# Create your models here.
class CategoriaProducto(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=15)
    def __str__(self):
        return str(self.descripcion)

class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    id_categoria = models.ForeignKey(CategoriaProducto,on_delete=models.CASCADE,)
    nombre = models.CharField(max_length=25)
    imagen = models.ImageField(upload_to='productos',null=True)
    precio = models.PositiveIntegerField()
    estado = models.CharField(max_length=15,default="habilitado")
    def __str__(self):
        return str(self.nombre)

class Carrito(models.Model):
    id = models.AutoField(primary_key=True)
    id_producto = models.ForeignKey(Producto,on_delete=models.CASCADE)
    cantidad = models.PositiveIntegerField()
    def __str__(self):
        return str(self.id)

class Mesa(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.PositiveIntegerField()
    def __str__(self):
        return str(self.numero)  

class Reserva(models.Model):
    id = models.AutoField(primary_key=True)
    mesa = models.ForeignKey(Mesa,on_delete=models.CASCADE)
    comensales= models.PositiveIntegerField()
    precio = models.PositiveIntegerField()
    fecha = models.DateTimeField()
    estado = models.CharField(max_length=15,default="solicitada")
    def __str__(self):
        return str(self.fecha)