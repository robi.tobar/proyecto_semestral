from django.urls import path
from django.urls import include
from .views import *

urlpatterns = [
    path('',home, name='home'),
    path('home',products, name='products'),
    path('perfil',perfil, name='perfil'),
    path('carrito',carrito, name='carrito'),
    path('admproductos',admproductos, name='admproductos'),
    path('crearproductos',crearproductos, name='crearproductos'),
    path('modificarproductos/<id>',modificarproducto, name='modificarproductos'),
    path('agregarprod/<id>/',agregarprod, name='agregarprod'),
    path('eliminarCarrito/<id>/',eliminarCarrito, name='eliminarCarrito'),
    path('eliminarProducto/<id>/',eliminarProducto, name='eliminarProducto'),
    path('reservar/',reservar, name='reservar'),
    path('addreserva/<id>/',addreserva, name='addreserva'),
    path('eliminarreserva/<id>/',eliminarreserva, name='eliminarreserva'),
    path('modreserva/<id>/',modreserva, name='modreserva'),
    path('crearcuenta',crearcuenta, name='crearcuenta'),
    path('admreserva',admreserva, name='admreserva'),
    path('addmesa',addmesa, name='addmesa'),
    path('quitamesa/<id>',quitamesa, name='quitamesa'),
    path('resolreserva/<id>',resolreserva, name='resolreserva'),
    path('estadoprod/<id>',estadoprod, name='estadoprod'),

]