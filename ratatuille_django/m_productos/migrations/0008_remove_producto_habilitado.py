# Generated by Django 3.2.5 on 2021-07-07 13:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('m_productos', '0007_alter_producto_habilitado'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producto',
            name='habilitado',
        ),
    ]
