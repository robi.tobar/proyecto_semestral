from django.contrib import admin
from django.db import models
from .models import *

# Register your models here.
class AdmCategoria(admin.ModelAdmin):
    list_display = ['id', 'descripcion']
    class Meta:
        model=CategoriaProducto
admin.site.register(CategoriaProducto,AdmCategoria)

class AdmProducto(admin.ModelAdmin):
    #con este segmento mostraremos todos los datos del producto en el mantenedor
    list_display = ['id','id_categoria','imagen','nombre','precio','estado']
    #con esto agregamos intrucciones para un filtro por NOMBRE
    list_filter = ['nombre']
    list_editable = ['nombre']
    class Meta:
        model=Producto
admin.site.register(Producto,AdmProducto)


class AdmCarrito(admin.ModelAdmin):
    list_display = ['id','id_producto','cantidad']
    list_editable = ['cantidad']
    class Meta:
        model=Carrito
admin.site.register(Carrito,AdmCarrito)

class AdmMesa(admin.ModelAdmin):
    list_display = ['id','numero']
    class Meta:
        model=Mesa
admin.site.register(Mesa,AdmMesa)

class AdmReserva(admin.ModelAdmin):
    list_display = ['id','mesa','comensales','precio','fecha']
    class Meta:
        model=Reserva
admin.site.register(Reserva,AdmReserva)
