from .forms import *
from django.shortcuts import get_object_or_404, redirect, render
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.db.models import Sum


# Create your views here.
def home(request):
   productos = Producto.objects.all()
   categorias = CategoriaProducto.objects.all()
   contexto = {
      'lista': productos,
      'categorias' : categorias 
   }

   return render(request,'m_productos/home.html',contexto)

def products(request):
   query = Producto.objects.all()
   carrito = Carrito.objects.count()
   categorias = CategoriaProducto.objects.all()
   contexto = { 
      'categorias' : categorias,
      'query': query,
      'carrito': carrito
      }
   

   
   return render(request,'m_productos/home2.html',contexto)

#agregar productos al carrito
def agregarprod(request, id):
   id_producto = id
   #icono del carrito
   carrito = Carrito.objects.count()
   data = { 
      'id_producto' : id_producto,
      'cantidad' : 1
   }
   prodCart = frmProductoCarro(data)
   contexto = {
      'frmCart' : prodCart,
      'carrito': carrito
   }
   if request.method == "POST":
      data = request.POST
      prodCart = frmProductoCarro(request.POST or None)
      if prodCart.is_valid() :
         messages.add_message (request, messages.INFO, "Producto agregado al carrito")
         prodCart.save()
         prodCart.clean()
         return redirect(to="products") 

   return render(request, 'm_productos/agregarCarrito.html',contexto)

def perfil(request):
   carrito = Carrito.objects.count()
   contexto={
      'carrito': carrito

   }
   return render(request,'m_productos/perfil.html',contexto)

def carrito(request):
   query = Carrito.objects.all()
   sub_t = 0
   carrito = Carrito.objects.count()
   
   #sub_t = query.aggregate(Sum('precio'))
   contexto={
      'query' : query,
      'carrito': carrito,
      'sub_t':sub_t}
   if request.method == "POST":
      query.delete()
   return render(request,'m_productos/carrito.html',contexto)

def admproductos(request):
   productos = Producto.objects.all()
   contexto = {'lista':productos}
   return render(request,'m_productos/admProductos.html',contexto)  

def crearproductos(request):
   producto=frmProducto(request.POST or None)
   contexto={
      'formulario':producto #tag que sera llamado en el HTML
   }
   if request.method == "POST":
      data=request.POST
      files=request.FILES
      producto=frmProducto(data, files)
      if producto.is_valid():
         producto.save()
         producto.clean()
         contexto={
            'mensaje':"producto agregado",
         }
         return render(request, 'm_productos/crearProducto.html',contexto)
      
   return render(request, 'm_productos/crearProducto.html',contexto)

def modificarproducto(request,id):
   productos = get_object_or_404(Producto, id = id)
   producto=frmModProducto(instance=productos)
   #producto.fields['id_categoria'].widget.attrs['disabled'] = 'disabled'
   #producto.fields['id_categoria'].as_hidden
   

   contexto = {
      'producto' : productos,
      'formulario' : producto,
   }
   #ahora preguntamos si el metodo de envio cambio de get a post
   if request.method == "POST": #lo que se traduce en si el boton submit fue activado
      #producto.fields['id_categoria'].disabled=False
      data=request.POST
      files=request.FILES
      producto=frmModProducto(data,files,instance=productos)
      if producto.is_valid():
         producto.save()
         producto.clean()
         return redirect(to="admproductos") 

   return render(request,'m_productos/modificarProducto.html',contexto)

def eliminarCarrito(request, id):
   producto = get_object_or_404(Carrito, id = id)
   producto.delete()
   return redirect(to='carrito')

def eliminarProducto(request, id):
   producto = get_object_or_404(Producto, id = id)
   contexto = {
      'producto' : producto,
      'mensaje' : 'hola'
   }
   if request.method == "POST":
      producto.delete()
      contexto= {
         'mensaje' : 'Producto Eliminado Exitosamente'
      }
      return render(request, 'm_productos/eliminarProd.html',contexto)
   return render(request, 'm_productos/eliminarProd.html',contexto)

def reservar(request):
   reservas = Reserva.objects.all()
   mesas = Mesa.objects.all()
   contexto = {
      'reservas' : reservas,
      'mesas' : mesas
      }
   return render(request,'m_productos/reservar.html',contexto)

def addreserva(request,id):
   formulario = frmReserva(data = {'mesa' : id})
   contexto = {
      'mesa' : id,
      'formulario' : formulario
   }
   if request.method == "POST":
      formulario = frmReserva(request.POST or None)
      if formulario.is_valid() :
         mesa = id
         comensales = formulario.cleaned_data['comensales']
         if comensales <= 3:
            precio = 7000
         elif comensales > 3 and comensales <= 5:
            precio = 5000
         else:
            precio = 12000
         fecha = formulario.cleaned_data['fecha']
         mensaje = 'Reserva Agendada exitosamente'
         data = {
            'mesa' : mesa,
            'comensales' : comensales,
            'precio' : precio,
            'fecha' : fecha
         }
         fmrR = frmR(data)
         contexto = {
            'messages' : messages
         }
         fmrR.save()
         fmrR.clean()
         messages.add_message (request, messages.INFO, mensaje) 
         return redirect(to='reservar')
   return render(request,'m_productos/addreserva.html',contexto)

def eliminarreserva(request, id):
   reserva = get_object_or_404(Reserva, id = id)
   contexto = {
      'reserva' : reserva
   }
   if request.method == "POST":
      reserva.delete()
      return redirect(to='reservar')
   return render(request,'m_productos/eliminarreserva.html',contexto)

def modreserva(request, id):
   reserva = get_object_or_404(Reserva, id = id)
   formulario = frmR2(instance=reserva)
   contexto = {
      'formulario' : formulario,
      'reserva' : reserva
   }
   if request.method == "POST":
      data=request.POST
      formulario = frmR2(data, instance=reserva)
      if formulario.is_valid() :
         mensaje = 'fecha de Reserva modificada exitosamente'
         messages.add_message (request, messages.INFO, mensaje)
         formulario.save()
         formulario.clean()
         return redirect(to='reservar')

   return render(request,'m_productos/modreserva.html', contexto)

def crearcuenta(request):
   formulario = frmUsuario
   contexto = {
      'formulario' : formulario
   }
   return render(request, 'registration/crearcuenta.html',contexto)

def admreserva(request):
   reservas = Reserva.objects.all()
   mesas = Mesa.objects.all()
   contexto = {
      'reservas' : reservas,
      'mesas' : mesas
      }
   return render(request,'m_productos/admreserva.html',contexto)

def addmesa(request):
   mesa=frmMesa(request.POST or None)
   contexto={
      'formulario':mesa #tag que sera llamado en el HTML
   }
   if request.method == "POST":
      data=request.POST
      files=request.FILES
      mesa=frmMesa(data, files)
      if mesa.is_valid():
         mesa.save()
         mesa.clean()
         messages.add_message (request, messages.INFO, "Mesa agregada satisfactoriamente")
         return redirect(to=admreserva)
      
   return render(request, 'm_productos/addmesa.html',contexto)

def quitamesa(request,id):
   mesas = get_object_or_404(Mesa, id = id)
   contexto = {
      'mesas' : mesas
   }
   if request.method == "POST":
      mesas.delete()
      messages.add_message (request, messages.INFO, "Mesa Eliminada satisfactoriamente")
      return redirect(to='admreserva')
   return render(request,'m_productos/quitamesa.html',contexto)

def resolreserva(request, id):
   reserva = get_object_or_404(Reserva, id = id)
   form=frmEstadoRes(instance=reserva)
   contexto = {
      'reserva' : reserva,
      'formulario' : form,
   }
   if request.method == "POST": #lo que se traduce en si el boton submit fue activado
      #producto.fields['id_categoria'].disabled=False
      data=request.POST
      reserva=frmEstadoRes(data,instance=reserva)
      if reserva.is_valid():
         reserva.save()
         reserva.clean()
         messages.add_message (request, messages.INFO, "Reserva Resuelta Exitosamente")
         return redirect(to=admreserva)

   return render(request,'m_productos/resolreserva.html',contexto)

def estadoprod(request, id):
   producto = get_object_or_404(Producto, id = id)
   formulario=frmEstadoPro(instance=producto)
   contexto = {
      'producto' : producto,
      'formulario' : formulario
   }
   if request.method == "POST": #lo que se traduce en si el boton submit fue activado
      #producto.fields['id_categoria'].disabled=False
      data=request.POST
      formulario=frmEstadoPro(data,instance=producto)
      if formulario.is_valid():
         formulario.save()
         formulario.clean()
         return redirect(to="admproductos") 
   return render(request,'m_productos/estadoprod.html',contexto)
